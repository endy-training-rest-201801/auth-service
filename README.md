# Aplikasi Authorization Service #

* Melayani user login
* Menampilkan consent screen
* Menerbitkan `authorization_code`
* Menerbitkan `access_token` dan `refresh_token`
* Menyediakan jasa pengecekan token

## Flow Authorization Code ##

1. Jalankan aplikasi

        mvn spring-boot:run

2. Akses URL untuk otorisasi / consent screen

        http://localhost:10000/oauth/authorize?client_id=client001&redirect_uri=http://localhost:10000/handle-oauth-callback&response_type=code

3. Mendapatkan `authorization code` di URL callback

        http://localhost:10001/handle-oauth-callback?code=5oJh04

4. Tukarkan `authorization code` menjadi `access token` dan `refresh token`. Gunakan aplikasi HTTP client seperti [Postman](https://www.getpostman.com/) atau curl.

        curl -X POST -vu client001:abcd http://localhost:10000/oauth/token -d "client_id=client001&&redirect_uri=http://localhost:10001/handle-oauth-callback&grant_type=authorization_code&code=5oJh04"

    Output : 

    ```json
    {
        "access_token" : "6f4296c6-a922-40d9-aee9-cdc64cc9ec78",
        "token_type" : "bearer",
        "refresh_token" : "f97db216-1634-4feb-b035-5f876ed85e41",
        "expires_in" : 43199,
        "scope" : "view edit"
    }
    ```

    Postman :

    ![Postman Authcode](docs/img/01-postman-basic-auth.png)
    ![Postman Authcode](docs/img/02-postman-request-header.png)
    ![Postman Authcode](docs/img/03-postman-response.png)

5. Content token bisa diminta dengan cara request GET ke url check token `http://localhost:10000/oauth/check_token` dengan menggunakan Basic Auth.

        curl -vu client001:abcd http://localhost:10000/oauth/check_token?token=6f4296c6-a922-40d9-aee9-cdc64cc9ec78

    Outputnya seperti ini:

    ```json
    {
        "aud": [
            "belajar"
        ],
        "user_name": "user003",
        "scope": [
            "view",
            "edit",
            "approve"
        ],
        "active": true,
        "exp": 1538597661,
        "authorities": [
            "VIEW_TRANSAKSI",
            "APPROVE_TRANSAKSI"
        ],
        "client_id": "client001"
    }
    ```

## Flow Implicit ##

1. Jalankan aplikasi

        mvn spring-boot:run

2. Akses URL untuk otorisasi / consent screen

        http://localhost:10000/oauth/authorize?client_id=client002&redirect_uri=http://localhost:8080/&response_type=token

3. Mendapatkan `access_token` di URL redirect

        http://localhost:10001/handle-oauth-callback#access_token=d186e24c-d48f-470f-afb8-04f4d153def4&token_type=bearer&expires_in=43199&scope=read%20admin%20write

4. Parsing access token dari address bar browser dan simpan di `sessionStorage`

    ```js
    var getTokenFromUrl = function(){
        var token;
        var hashParams = window.location.hash.substr(1);
        if(!hashParams) {
            console.log("Tidak ada token di url");
            return;
        }
        console.log(hashParams);
        var eachParam = hashParams.split('&');
        for(var i=0; i<eachParam.length; i++){
            var param = eachParam[i].split('=');
            if('access_token' === param[0]) {
                token = param[1];
            }
        }
        console.log("Access Token : "+token);
        if(token){
            $window.sessionStorage.setItem('token', token);
        }
        $location.hash('');
    };
    ```

## JSON Web Token (JWT) ##

1. Generate keystore untuk menyimpan public key

        keytool -genkey -keystore src/main/resources/authstore.p12 -alias auth-service -storetype PKCS12 -keyalg RSA 

2. Jawab pertanyaan, isi passwordnya `abcd123`

        Enter keystore password: abcd123
        Re-enter new password: abcd123
        What is your first and last name?
        [Unknown]:  Auth Server
        What is the name of your organizational unit?
        [Unknown]:  Belajar OAuth
        What is the name of your organization?
        [Unknown]:  ArtiVisi
        What is the name of your City or Locality?
        [Unknown]:  Jakarta Timur
        What is the name of your State or Province?
        [Unknown]:  Jakarta
        What is the two-letter country code for this unit?
        [Unknown]:  ID
        Is CN=Auth Server, OU=Belajar OAuth, O=ArtiVisi, L=Jakarta Timur, ST=Jakarta, C=ID correct?
        [no]:  yes

3. Public key bisa diakses di `http://localhost:10000/oauth/token_key`

    ```json
    {
        "alg":"SHA256withRSA",
        "value":"-----BEGIN PUBLIC KEY----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlOJzUtkJF0L8dLm1Ia8HO65CJq+YGRXh5ygGgmY2UMeEbRyTlyuGvxPwC5VieTF2A12CZuZHIZi9YNGA0p5Gw7PExLkYTrjVymr5vhzNmX5o/98Gd7bBSgidNs4bPFoi1Dh7AdlTTgSobZRGvB9X5wyxYPNfE7BqQAlWwm/8fHDB61HMbACXbphSAspSYgfAcFrAj4wcO2HQH/Uil7ObC3O5co+Rcpc9qHuQ0KxOgh1WyZhGW7t3Oo+zYx4SAydxrlfZ/eue0nnubDoxkQFQjspF9qGhqI7k+9tGZrqdKl3ckaBQ9YtN5MkhF4S6dy5ywS3xMCBA3r72GK0/U/seMQIDAQAB\n-----END PUBLIC KEY-----"
    }
    ```

4. Setup di `resource-server`, di file `src/main/resources/application.properties`

        spring.oauth2.resource.jwt.key-value=-----BEGIN PUBLIC KEY----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlOJzUtkJF0L8dLm1Ia8HO65CJq+YGRXh5ygGgmY2UMeEbRyTlyuGvxPwC5VieTF2A12CZuZHIZi9YNGA0p5Gw7PExLkYTrjVymr5vhzNmX5o/98Gd7bBSgidNs4bPFoi1Dh7AdlTTgSobZRGvB9X5wyxYPNfE7BqQAlWwm/8fHDB61HMbACXbphSAspSYgfAcFrAj4wcO2HQH/Uil7ObC3O5co+Rcpc9qHuQ0KxOgh1WyZhGW7t3Oo+zYx4SAydxrlfZ/eue0nnubDoxkQFQjspF9qGhqI7k+9tGZrqdKl3ckaBQ9YtN5MkhF4S6dy5ywS3xMCBA3r72GK0/U/seMQIDAQAB\n-----END PUBLIC KEY-----

    atau cukup arahkan ke URL public key

        spring.oauth2.resource.jwt.key-uri=http://localhost:10000/oauth/token_key

5. Contoh Token JWT

    * Access Token : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiIHdyaXRlIiwicmVhZCJdLCJleHAiOjE1Mzc1NzU2MzcsImF1dGhvcml0aWVzIjpbIkNPTkZJR1VSRV9TWVNURU0iLCJWSUVXX0lORk8iXSwianRpIjoiMWFiZDYwNTEtYzQwNy00OWI1LThiNzctMTVkOGYwMGRmYjcxIiwiY2xpZW50X2lkIjoiY2xpZW50MDAxIn0.OLpgFFIPRQumik1Cp4F3WO5dzfWZ_nLUpV3ipxzdTTEO77a65lNX4-PtALkLPRMBg6Z56nEze5K0uMKDbutDk-PxL7rTEmKS1jZBCsMojdZyilBJ-ag2AWJMgIQERg3WF8o2iM2DZcp92VFMF9sAA23siz9nf3L5P8iJQ9m0DkVxW747IMYxMCrPb2BQTC2lm_ye1NTUD-aXIm_YQn3XCPD3Yg4j_jHU4mQV1FQdopx0ZfN9Md2VeeQE6nSFtJ3BxIvTRPdkTqdm5M3ahaBfr5BAR364a7bi5mdxEcIHiJ72wOS1P_Z73WJddTrPvoyt7g910rdHyiycTyWo1z8efw

    * Refresh Token : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiIHdyaXRlIiwicmVhZCJdLCJhdGkiOiIxYWJkNjA1MS1jNDA3LTQ5YjUtOGI3Ny0xNWQ4ZjAwZGZiNzEiLCJleHAiOjE1NDAxMjQ0MzcsImF1dGhvcml0aWVzIjpbIkNPTkZJR1VSRV9TWVNURU0iLCJWSUVXX0lORk8iXSwianRpIjoiNmQwMjhlN2UtN2Y3Ni00MDE0LTkwN2EtYTdiYWNmMzc5OWRiIiwiY2xpZW50X2lkIjoiY2xpZW50MDAxIn0.Cp93igq66P508EDLkKpGAjUG64dzweSALkkguSiATPEsZPmLxB1DpJ1VnbG2g2RTCczS-YhtbkwKzcMQVT98LwGf5gJfYHvBVeeMPYm82LA5wBNWGn702VQC9m9mLD_lkx_ldvx4MuFmpmALt8w4yhEE_hcCnF_yXacaJ1gyPiO-x7OL4KvPTgSO54YtgolRPO9QV6IbfEcB7sz_9jTLvi5CsQPdMpQNpx6gwg_b4VlpAK9yx-cPWZ2RAxs9J9JIcVVErxhidkHDrLv8Yc-175zyPoFI5qx9XKL_aXj_mdXZIySfi2iJBzVwANPmO8sZfPP_xOsFuW_rWe6aQBwFgg

6. JWT Token bisa dilihat isinya di [jwt.io](https://jwt.io)
